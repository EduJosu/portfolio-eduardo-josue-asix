Hola, este es mi perfil
Me llamo Eduardo Josué aunque me llaman Edu o Josu

nací en 2003 en guatemala pero vine a Terrassa en 2006 con 3 añitos y llevo toda mi vida aquí, tengo 19 años (en el momento en el que escribo esto)

Me considero una persona que le gusta reir y aunque a veces tiene la cabeza en las nubes está dispuesto a trabajar, me considero alguien amable, bastante energico y tambien respetuoso hacia las otras personas

Estos son mis estudios:

2007-2015 Escola Serra de l'obac

2015-2019 Instuto Can Roca (ESO)

2019-2022 Instituto Nicolau Copernic (Grado medio SMIX)

2022-Actualidad Instituto Nicolau Copernic (Grado Superior ASIX)

Se hablar 3 lenguas, catalán, castellano e inglés

Se me puede contactar de las siguientes maneras:
Mail: ejgonzalez@alumnat.copernic.cat

Es un gusto que hayas podido leer esto